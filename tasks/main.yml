---

  - include_vars: 'vars/main.yml'


  - name: Get release timestamp
    command: date +%Y%m%d%H%M%S
    register: timestamp

  - set_fact:
      release_path: "{{project_root}}/releases/{{ timestamp.stdout }}"
      release_name: "{{app_name}}_{{project_environment}}_{{timestamp.stdout}}"

  - name: Create release directory
    file: >
      state=directory
      owner={{app_user}}
      group={{app_group}}
      recurse=yes
      path={{ release_path}}

  - name: Create application subdirectories  {{release_path}}
    file: path={{release_path}}/{{item}}/ owner={{app_user}} group={{app_group}} state=directory
    with_items:
      - config
      - data
      - data/filestore
      - var
      - var/log

  - name: Create virtualenvironment
    command:
      cmd: virtualenv {{release_path}}/virtualenv -p python3
      creates: "{{release_path}}/virtualenv"

  - name: Upload sources
    synchronize:
      archive: yes
      compress: yes
      copy_links: no
      delete: yes
      src: "{{ playbook_dir }}/../../src/"
      dest: '{{release_path}}/src'
      rsync_opts:
        - '--exclude=*~'
        - '--exclude=.idea'
        - '--exclude=.git*'
        - '--exclude=README.md'
        - '--exclude=release.yml'

  - name: Install Pip Dependencies
    pip: name={{pip_requirements}} executable={{release_path}}/virtualenv/bin/pip
    when: pip_requirements is defined and pip_requirements | length > 0

  - name: Installing Core Odoo Dependencies
    pip: requirements="{{odoo_core}}/requirements.txt" executable={{release_path}}/virtualenv/bin/pip

  - name: Installing Our Module Dependencies
    pip: requirements={{release_path}}/src/project/requirements.txt executable={{release_path}}/virtualenv/bin/pip

  - name: Upload odoo configuration {{odoo_config_template}}
    template: src={{odoo_config_template}} dest={{release_path}}/config/odoo.conf

  - name: Find out current release directory
    command: readlink "{{project_root}}/current"
    register: app_current_path
    ignore_errors: True

  - command: cat '{{app_current_path.stdout}}/release.yml'
    register: has_previous_release_file
    ignore_errors: True

  - set_fact:
      previous_release: '{{has_previous_release_file.stdout | from_yaml }}'
    when: not has_previous_release_file.failed

  - name: Stop Previous Instance {{previous_release.name}}
    supervisorctl: name={{previous_release.name}} state=stopped
    ignore_errors: True
    when: previous_release is defined

  - name: Create database if it doesn't exist
    include_tasks: "init.yml"
    when: previous_release is not defined

  - name: Replicate previous release
    include_tasks: "replicate.yml"
    when: previous_release is defined and app_replicate

  - name: Upgrade all modules
    shell: "{{release_path}}/virtualenv/bin/python3 {{odoo_core_bin}} -c {{release_path}}/config/odoo.conf --xmlrpc-port={{app_port}} --longpolling-port={{app_long_port}} -d {{release_name}} -u all --stop-after-init"

  - name: Install required modules
    shell: "{{release_path}}/virtualenv/bin/python3 {{odoo_core_bin}} -c {{release_path}}/config/odoo.conf --xmlrpc-port={{app_port}} --longpolling-port={{app_long_port}} -d {{release_name}} -i {{ app_install_modules | join(',') }} --stop-after-init"
    when: app_install_modules is defined and app_install_modules | length > 0

  - name: Upload supervisor configuration {{supervisor_config_template}}
    template: src={{supervisor_config_template}} dest={{supervisor_config}}/{{release_name}}.conf

  - name: Reload Supervisor to get new configuration
    supervisorctl: name={{release_name}} state=present

  - name: Link to current
    file: src={{release_path}} dest={{project_root}}/current state=link force=yes

  - name: Mark Release
    template: src=files/release.yml  dest={{release_path}}/release.yml

  - name: Start {{release_name}}
    supervisorctl: name={{release_name}} state=started

  - name: List old releases and clean them up
    shell: "ls -t {{project_root}}/releases/ | tail -n +4"
    register: previous_releases

  - name: Cleaning up old releases
    include_tasks: cleanup-release.yml
    with_items: '{{previous_releases.stdout_lines}}'

